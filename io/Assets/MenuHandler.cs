﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuHandler : MonoBehaviour {

	public NetManBehaviour manager;
	public Text ipInput;
	public Text nameInput;


	public void HandleHost()
    {
		manager.playerName = nameInput.text;
		manager.ServerChangeScene("main scene");
		manager.StartHost();
    }
	public void HandleJoin()
    {
		manager.playerName = nameInput.text;
		manager.networkAddress = ipInput.text;
		SceneManager.LoadScene("main scene", LoadSceneMode.Single);
		manager.StartClient();
    }
	

}
