﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;

public class Health : NetworkBehaviour {

    public GameObject coin;
    GameObject character;
    public const int maxHealth = 100;

    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth = maxHealth;
    public bool destroyOnDeath;

    public RectTransform healthBar;

    private DateTime lastHitTime;
    private Boolean isDead = false;

    void Start()
    {
        character = gameObject.transform.Find("Character").gameObject;
        FloatingTextController.Initialize();
        InvokeRepeating("HealthRegen", 0.0f, 5f);
    }
    void HealthRegen()
    {
        DateTime currTime = DateTime.Now;
        Double timeDelta = (currTime - lastHitTime).TotalMilliseconds;

        if (timeDelta > 3000)
        {
            currentHealth += 10;
            currentHealth = Math.Min(maxHealth, currentHealth);
        }
    }

    [Command]
    public void CmdTakeDamage(int amount)
    {
        if (!isServer)
            return;

        if (amount > 0)
        {
             DateTime currTime = DateTime.Now;
        }
       

        currentHealth -= amount;

        if (amount > 15){
            this.GetComponent<CharController>().RpcRegisterHit();
        }

        if (currentHealth <= 0 && !this.isDead)
        {
            this.isDead = true;
            this.deathCoins(character.transform.position, 5);

            var npcController = this.GetComponent<NpcController>();

            if (npcController)
            {
                Destroy(character.GetComponent<HingeJoint2D>());
            }

            StartCoroutine(doDeath());
        }
    }

    private void deathCoins(Vector2 pos, int amount)
    {
        for (int i = 0; i<amount; i++)
        {
            pos.x += UnityEngine.Random.Range(-3.0f, 3.0f);
            pos.y += UnityEngine.Random.Range(-2.0f, 2.0f);
            var coinInst = (GameObject)Instantiate(coin, pos, transform.rotation);
            NetworkServer.Spawn(coinInst);
        }
    }
    IEnumerator doDeath()
    {
        yield return new WaitForSeconds(1);
        if (destroyOnDeath)
        {
            Destroy(gameObject);
        }
        else
        {
            currentHealth = maxHealth;
            isDead = false;
            this.GetComponent<CharController>().RpcRespawn();
        }
    }

    void OnChangeHealth (int health)
    {
        if (healthBar  != null){
            healthBar.sizeDelta = new Vector2(health, healthBar.sizeDelta.y);
        }
    }


}
