﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

[NetworkSettings(channel = 1, sendInterval = 0.01666666666f)]
public class CharController : NetworkBehaviour {
	Rigidbody2D rb;
	Rigidbody2D rb2;
	Collider2D playerHitBox;
	public GameObject character;
	public GameObject sword;
	public GameObject LocalPlayerController;
	public GameObject nameField;

	private SpriteRenderer charSprite;

	private NetManBehaviour netman;

	private DateTime lastHit;

	NetworkIdentity networkIdentity;

	//mirrors to all clients
	[SyncVar]
	public string playerName;

	[SyncVar]
	public string state = "normal";

	[SyncVar]
	public int coins;


	private void Start() {
		lastHit = DateTime.Today;
		character = gameObject.transform.Find("Character").gameObject;
		sword = gameObject.transform.Find("Sword").gameObject;
		rb = character.GetComponent<Rigidbody2D>();
		rb2 = sword.GetComponent<Rigidbody2D>();
		charSprite  = character.GetComponent<SpriteRenderer>();
		if(isLocalPlayer) {
	  		LocalPlayerController = GameObject.Find("LocalPlayerController");
			LocalPlayerController.GetComponent<LocalPlayerController>().Player = gameObject;
	  	}
		networkIdentity = GetComponent<NetworkIdentity>();
		netman = GameObject.Find("NetworkManager").GetComponent<NetManBehaviour>();

		string[] dude_sprites = new string[] {"dude_pale", "dude_mid", "dude_dark"};
		string dude = dude_sprites[Random.Range(0, dude_sprites.Length)];

		var dude_live_sprite = character.GetComponent<SpriteRenderer>();
		dude_live_sprite.sprite = Resources.Load<Sprite>("Sprites/"+dude);
  	}

  void Update () {
		if(isLocalPlayer){
			doSync();
		}
		/*Move this out of update it's just here for testing - no*/
		if(isLocalPlayer){
			string newName = netman.playerName;
			if(playerName != newName){
				CmdMyNameIs(newName);
			}
		}
		nameField.GetComponent<Text>().text = playerName;
		/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

		if (state == "normal")
		{
			charSprite.color = Color.white;
		}
		else if (state == "dashing")
		{
			charSprite.color = Color.red;
		}
		else if (state == "disabled")
		{
			charSprite.color = Time.frameCount % 2 == 0 ? Color.grey : Color.white;
		}
		else if (state == "dashless")
		{
			charSprite.color = Color.gray;
		}

		DateTime currTime =  DateTime.Now;
		Double timeDelta = (currTime - lastHit).TotalMilliseconds;
		if (timeDelta < 1000)
		{
			CmdSetState("disabled");
			return;
		}
		else
		{
			CmdSetState("normal");		//this is spamming an error about authority, should it only run for the owned player? use if(isLocalPlayer){
		}
	}
	public void doSync()
	{
		if (!isServer)
			CmdSyncRigidbody(rb.velocity, rb2.velocity, rb.angularVelocity, rb2.angularVelocity, character.transform.position, sword.transform.position, character.transform.rotation, sword.transform.rotation);
		else
			RpcSyncRigidbody(rb.velocity, rb2.velocity, rb.angularVelocity, rb2.angularVelocity, character.transform.position, sword.transform.position, character.transform.rotation, sword.transform.rotation);
	}

	//serverside name change
	[Command]
	void CmdMyNameIs(string name) {
		playerName = name;
	}

	[Command]
	public void CmdSetState(string stateName) {
		state = stateName;
	}

	[ClientRpc]
	public void RpcRegisterHit()
	{
		lastHit = DateTime.Now;
	}

	[ClientRpc]
	public void RpcRespawn()
	{
		// I hope you are a wizard
		// Because I've got some magic numbers for you
		character.transform.position = new Vector3(-2.214084f, 2.056828f, 0);
		sword.transform.position = new Vector3(-0.38f, 2.056828f, 0);
		character.transform.rotation = Quaternion.Euler(0,0,0);
		sword.transform.rotation = Quaternion.Euler(0,0,0);

		charSprite.color = Color.black;
		rb.velocity = Vector2.zero;
		character.transform.right = Vector3.zero;
		sword.GetComponent<Sword>().resetLength();
		coins = 0;
    }

	[Command]
	void CmdSyncRigidbody(Vector2 vel1, Vector2 vel2, float angvel1, float angvel2, Vector2 pos1, Vector2 pos2, Quaternion rot1, Quaternion rot2){
		RpcSyncRigidbody(vel1, vel2, angvel1, angvel2, pos1, pos2, rot1, rot2);
	}

	[ClientRpc]
	void RpcSyncRigidbody(Vector2 vel1, Vector2 vel2, float angvel1, float angvel2, Vector2 pos1, Vector2 pos2, Quaternion rot1, Quaternion rot2){
		if(!isLocalPlayer){
			rb.velocity = vel1;
			rb.angularVelocity = angvel1;
			rb2.velocity = vel2;
			rb2.angularVelocity = angvel2;
			character.transform.position = pos1;
			character.transform.rotation = rot1;
			sword.transform.position = pos2;
			sword.transform.rotation = rot2;
		}
	}

	[ClientRpc]
	public void RpcLengthenSword(int amount){
		sword.GetComponent<Sword>().lengthen(amount);
	}
}
