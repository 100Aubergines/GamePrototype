﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkEffect : MonoBehaviour {

	public Sprite sprite;
	SpriteRenderer sp;
	public float speed;
	public float bananaAmount;

	void remove () {
		speed = 0;
		Invoke("remove2", 0.2f);
	}

	void remove2 () {
		Destroy(gameObject);
	}

	void Start () {
		sp = GetComponent<SpriteRenderer>();
		sp.sprite = sprite;
		Invoke("remove", 0.3f + Random.Range(0.0f, 0.3f));
		transform.Rotate(Vector3.forward * Random.Range(0f, 360f));
		speed = Random.Range(8f, 12f);
		bananaAmount = Random.Range(-60f, 60f);
	}

	void Update () {
		transform.Translate(Vector2.right * Time.deltaTime * speed);
		speed = speed * 0.99f;
		transform.Rotate(Vector3.forward * bananaAmount * Time.deltaTime);
	}
}
