﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Scoreboard : MonoBehaviour {

	Text scoreText;


	struct scoreInfo {
		public string name;
		public int score;
	}

	void Start () {
		scoreText = GetComponent<Text>();
		InvokeRepeating("checkScores", 1f, 0.5f);
	}

	void checkScores () {
		string scoreTextString = "";
		List<GameObject> players = new List<GameObject>();
		List<scoreInfo> scores = new List<scoreInfo>();
		Object[] obj = GameObject.FindObjectsOfType(typeof(Transform));
		foreach(Transform o in obj){
			if(o.gameObject.GetComponent<CharController>() != null && o.gameObject.GetComponent<NpcController>() == null){ //its a player and not a bot
				players.Add(o.gameObject);
				}
		}
		foreach(GameObject o in players){
			scoreInfo s;
			s.name = o.GetComponent<CharController>().playerName;
			s.score = o.GetComponent<CharController>().coins;
			scores.Add(s);
		}
		for(int n = 0; n < scores.Count; n++){				//bubble sort
			for(int i = 0; i < scores.Count - 1; i++){
				if(scores[i].score < scores[i + 1].score){
					scoreInfo temp = scores[i];
					scores[i] = scores[i + 1];
					scores[i + 1] = temp;
				}
			}
		}
		foreach(scoreInfo s in scores){
			scoreTextString += s.name + ": " + (s.score.ToString()) + "\n";
			Debug.Log(s.score + " aaa " + s.score.ToString());
		}
		scoreText.text = scoreTextString;
		Debug.Log(scoreTextString);
	}
}
