﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CoinSpawner : NetworkBehaviour {

	public GameObject coin;
	public float arenax;	//how far across the arena is
	public float arenay;	//used for setting the coin spawn position so this object should be placed in the middle of the arena
	public float spawnFrequency;

	void Start () {
		if(!isServer){
			Destroy(gameObject);
		}
		InvokeRepeating("spawnCoins", 0f, spawnFrequency); //run on start then every 5s
	}

	private void spawnCoins() {
		Vector2 pos = new Vector2((transform.position.x - (arenax / 2)) + Random.Range(0f, 1f) * arenax, (transform.position.y - (arenay / 2)) + Random.Range(0f, 1f) * arenay);
		var coinInst = (GameObject)Instantiate(coin, pos, transform.rotation);
		NetworkServer.Spawn(coinInst);
	}
}
