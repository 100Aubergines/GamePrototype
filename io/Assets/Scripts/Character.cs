﻿using UnityEngine;
using System.Collections;
using System;

public class Character : MonoBehaviour {
    public AudioClip kiss;
    private AudioSource audioSource;
    private DateTime lastHitTime;

     void OnCollisionEnter2D(Collision2D collision)
    {
        var hit = collision.gameObject;
        var health = hit.GetComponentInParent<Health>();

        if (hit.name == "Character" && health  != null)
        {
            
            DateTime currTime =  DateTime.Now;
            Double timeDelta = (currTime - lastHitTime).TotalMilliseconds;
            if (timeDelta > 1000)
            {
                lastHitTime = currTime;
                FloatingTextController.CreateFloatingText("❤️", hit.transform);
                PlayKissSound();
            }
        }
    }
    
    private void PlayKissSound()
    {
        audioSource.clip = kiss;
        audioSource.Play();
    }
}