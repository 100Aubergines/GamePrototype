using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MouseController : NetworkBehaviour {
	GameObject character;
	GameObject sword;
	Rigidbody2D rb;

	CharController charController;
	public float playerBoost = 100f;

	private void Start() {
		charController = this.GetComponent<CharController>();
		character =  charController.character;
		sword = 	 charController.sword;
		rb = character.GetComponent<Rigidbody2D>();
	}

	void Update () {
		if(Application.isFocused && isLocalPlayer) {
			Vector3 mp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			float dist = Mathf.Abs(Vector2.Distance(mp, character.transform.position));
			dist = Mathf.Min(1, dist);
			PUpdate(new Vector3(mp.x - character.transform.position.x, mp.y - character.transform.position.y, 0), (character.transform.right * dist * Time.deltaTime * 100));
    	}
	}
	void PUpdate (Vector3 v3, Vector2 v2) {
		if (charController.state == "disabled")
		{
			return;
		}
		
		int ACCELERATION = 50;
		float MAX_SPEED = 2f;

		if(playerBoost < 90)
		{
			charController.CmdSetState("dashless");
		}
		else
		{
			charController.CmdSetState("normal");
		}

		if (Input.GetMouseButton(0) && playerBoost > 0)
		{
			playerBoost -= 120 * Time.deltaTime; //Reduce boost bar
			ACCELERATION /= 4;
			MAX_SPEED *= 4;
			charController.CmdSetState("dashing");
		}
		else if (playerBoost < 100)
		{
			playerBoost += 60 * Time.deltaTime; //Replenish boost bar
		}

		character.transform.right = v3;

		//TODO THIS AS AN APPLY FORCE
		rb.velocity += v2 * Time.deltaTime * ACCELERATION;
		if (rb.velocity.magnitude > MAX_SPEED){
			//Gradually reduce speed
			rb.velocity = ((rb.velocity.normalized*rb.velocity.magnitude) + (rb.velocity.normalized * MAX_SPEED))/2;
		}
	}
}
