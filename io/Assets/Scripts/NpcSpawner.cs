﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


[NetworkSettings(channel = 0, sendInterval = 1.0f)]

public class NpcSpawner : NetworkBehaviour {

    public GameObject enemyPrefab;
    public int numberOfEnemies;

	void Start()
    {
        InvokeRepeating("spawnNPCs", 0f, 5f); //run on start then every 5s
    }

	private void spawnNPCs()
	{
		int npcCount = GameObject.FindObjectsOfType(typeof(NpcController)).Length;
		if (npcCount >= numberOfEnemies)
			return;


		string[] names = new string[] {
			"Bot Maximus",
			"Bot Commodus",
			"Bot Lucilla",
			"Bot Proximo",
			"Bot Juba",
			"Bot Hagen",
			"Bot Quintus",
			"Bot Cicero",
			"Bot Russell",
			"Bot Crow"
		};
		var start = Random.Range(0, names.Length);

        for (int i=npcCount; i < numberOfEnemies; i++)
        {
            var spawnPosition = new Vector2(
                Random.Range(-20.0f, 20.0f),
                Random.Range(-5.0f, 5.0f));

            var spawnRotation = Quaternion.Euler(
                0.0f,
                0.0f,
                0.0f);

			int nameIdx = (start + i) % names.Length;
			string name = names[nameIdx];


            var enemy = (GameObject)Instantiate(enemyPrefab, spawnPosition, spawnRotation);
			enemy.GetComponent<CharController>().playerName = name;
		    NetworkServer.Spawn(enemy);
        }
	}
}
