﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	
	public GameObject LocalPlayerController;
	public GameObject player;
	public float verticalDeadzone = 0.3f; //the portion of the screen width/height in the centre
	public float horizontalDeadzone = 0.3f; //where the player will not move the camera
	public float cameraSpeed = 1;
    public AudioClip musicLoop;
    private AudioSource audioSource;

	void Start () {
		LocalPlayerController = GameObject.Find("LocalPlayerController");
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
	}
	
	void Update () {
		if(!player){
			if(LocalPlayerController.GetComponent<LocalPlayerController>().Player){
				player = LocalPlayerController.GetComponent<LocalPlayerController>().Player.GetComponent<CharController>().character;
			}
		} else {
			Vector2 pos = Camera.main.WorldToScreenPoint(new Vector3(player.transform.position.x, player.transform.position.y, 0));
			if(pos.x < (Screen.width - (Screen.width * horizontalDeadzone)) / 2) {
				transform.Translate(Vector3.left * (((Screen.width - (Screen.width * horizontalDeadzone)) / 2) - pos.x) * cameraSpeed);
			}
			if(pos.x > (Screen.width + (Screen.width * horizontalDeadzone)) / 2) {
				transform.Translate(Vector3.right * (pos.x - (Screen.width + (Screen.width * horizontalDeadzone)) / 2) * cameraSpeed);
			}
			if(pos.y < (Screen.height - (Screen.height * verticalDeadzone)) / 2) {
				transform.Translate(Vector3.down * (((Screen.height - (Screen.height * verticalDeadzone)) / 2) - pos.y) * cameraSpeed);
			}
			if(pos.y > (Screen.height + (Screen.height * verticalDeadzone)) / 2) {
				transform.Translate(Vector3.up * (pos.y - (Screen.height + (Screen.height * verticalDeadzone)) / 2) * cameraSpeed);
			}
		}
	}
}
