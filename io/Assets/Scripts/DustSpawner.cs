﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustSpawner : MonoBehaviour {
	public GameObject dust;
	void Start () {
		InvokeRepeating("spawn", 0f, 1f);
	}

	void spawn () {
		if(Random.Range(0.0f, 15.0f) > 14f){
			Instantiate(dust, transform.position, transform.rotation);
		}
	}
}
