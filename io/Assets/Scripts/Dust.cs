﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dust : MonoBehaviour {
	public float speed = 3;

	void Start () {
		speed += Random.Range(-0.5f, 0.5f);
		Destroy(gameObject, 60f);
	}

	void Update () {
		transform.Translate(Vector2.left * Time.deltaTime * speed);
	}
}
