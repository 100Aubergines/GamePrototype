﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameplateTransform : MonoBehaviour {
	public float verticalOffset = 0.8f;
	void LateUpdate () {
		transform.rotation = Quaternion.identity;
		transform.position = new Vector2(transform.parent.parent.position.x, transform.parent.parent.position.y + verticalOffset);
	}
}
