﻿using UnityEngine;
using System.Collections;
using System;

public class Sword : MonoBehaviour {
    private AudioSource audioSource;
    private DateTime lastHitTime;
    public GameObject BloodEffect;
    public GameObject SparkEffect;
    public AudioClip swordHit;
    public bool stretchySword; //check this for a sword that gets longer, uncheck it for a sword of a fixed size
    public GameObject blade;
    public GameObject point;
    public BoxCollider2D hitbox;
    public float length;
    float damageScale = 4;
    Rigidbody2D rb;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        if(stretchySword){
            damageScale = 3;
            blade = transform.Find("Blade").gameObject;
            point = transform.Find("Point").gameObject;
            hitbox = GetComponent<BoxCollider2D>();
        }
        audioSource = GetComponent<AudioSource>();
	      lengthen(1);
    }

    public void lengthen (int amount) {
      if(!stretchySword){
        return;
      }
      float magicScale = 0.016f;
      for(int i = 0; i < amount; i++){
        //lengthen the blade sprite
        blade.transform.localScale = new Vector3(blade.transform.localScale.x + magicScale, 1, 1);
        //put the tip in the right place      (for every 1.6 worth of blade x scale, position the tip at x -0.435 + 1.073)
        point.transform.localPosition = new Vector3((-0.435f + ((blade.transform.localScale.x / 1.6f) * 1.073f)), 0, 0);
        //lengthen the collider               (for each 1.6 of blade x, the collider is 0.5 + 1.035x wide)
        hitbox.size = new Vector3((0.5f + ((blade.transform.localScale.x / 1.6f) * 1.035f)), 0.3f, 0);
        //offset the collider                  (and the x offset is -0.5 + 0.5175x)
        hitbox.offset = new Vector2((-0.5f + ((blade.transform.localScale.x / 1.6f) * 0.5175f)), 0);
        //increase the mass                    (1.6 blade = 0.25 mass)
        rb.mass = blade.transform.localScale.x / 1.6f * 0.25f;
        //set the new damage scale		(3 damage per 1.6 sword length)
        damageScale = (blade.transform.localScale.x / 1.6f) * 3f;
      }
    }

    public void resetLength () {
      blade.transform.localScale = new Vector3(1.6f, 1, 1);
      lengthen(1);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        var hit = collision.gameObject;
        var health = hit.GetComponentInParent<Health>();

        if (hit.name == "Character" && health != null) {

            DateTime currTime = DateTime.Now;
            Double timeDelta = (currTime - lastHitTime).TotalMilliseconds;
            if (timeDelta > 100) {
                int damage = (int)(collision.relativeVelocity.magnitude * damageScale);
                health.CmdTakeDamage(damage);
                lastHitTime = currTime;

                //blood effects
                int p = (int)(UnityEngine.Random.Range(1.0f, 1.0f + collision.relativeVelocity.magnitude));
                for (int i = 0; i < p; i++) {
                    GameObject b = Instantiate(BloodEffect, collision.transform.position, Quaternion.identity);
                    b.transform.Rotate(0, 0, (Mathf.Atan2(collision.transform.position.y - transform.position.y, collision.transform.position.x - transform.position.x) * 57.296f) + UnityEngine.Random.Range(-30.0f, 30.0f)); //amazing
                    b.GetComponent<BloodEffect>().speed = UnityEngine.Random.Range(1.0f, collision.relativeVelocity.magnitude);
                }

                FloatingTextController.CreateFloatingText((-damage).ToString(), hit.transform);
            }
        }
        PlayHitSound();
        int c = (int)(collision.relativeVelocity.magnitude / 8);
        if (c > 0) {    //sparks depending on force
            for (int i = 0; i < c; i++) {
                Instantiate(SparkEffect, transform.position, Quaternion.identity);
            }
        }
    }

    private void PlayHitSound()
    {
        audioSource.clip = swordHit;
        audioSource.Play();
    }
}
