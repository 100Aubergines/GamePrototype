﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class NpcController : NetworkBehaviour {

	// Use this for initialization
	GameObject character;
	GameObject sword;
	Rigidbody2D rb;

	public GameObject farCollider;

	CharController charController;
	public float playerBoost = 100f;

	int frameCountOffset = 0;
	public string state = "idle";
	private DateTime lastStateChange;

	Vector2 goalPos;
	Vector2 futureGoalPos;
	double lastDistance = 0;

	private void Start() {
		charController = this.GetComponent<CharController>();
		character =  charController.character;

		//sword = 	 charController.sword;
		rb = character.GetComponent<Rigidbody2D>();
	
		goalPos = character.transform.position;
		futureGoalPos = goalPos;

		frameCountOffset = UnityEngine.Random.Range(0, 59);
	}
	void Update () {
		if (!isServer)
            return;

			DateTime currTime =  DateTime.Now;
            Double timeDelta = (currTime - lastStateChange).TotalMilliseconds;

			if (timeDelta > 900){
				lastDistance = 0;

				if (state != "idle")
				{
					state = "idle";
					lastStateChange = currTime;
					return;
				}
			}
			

			if (state == "idle" && (Time.frameCount+frameCountOffset) % 120 == 0)
			{
				var x = character.transform.position.x + UnityEngine.Random.Range(-5, 5);
				var y = character.transform.position.x + UnityEngine.Random.Range(-5, 5);
				futureGoalPos += new Vector2(x, y);
			}


			//Goal pos is the imgaginary mouse position
			goalPos = (futureGoalPos + goalPos)/2; //Slowly change the goalPos

			float dist = Mathf.Abs(Vector2.Distance(goalPos, character.transform.position));
			dist = Mathf.Min(1, dist);
			PUpdate(new Vector3(goalPos.x - character.transform.position.x, goalPos.y - character.transform.position.y, 0), (character.transform.right * dist * Time.deltaTime * 100));
	
			charController.doSync();
	}
	void PUpdate (Vector3 v3, Vector2 v2) {
		if (charController.state == "disabled")
		{
			return;
		}
		int ACCELERATION = 50;
		float MAX_SPEED = 2f;

		character.transform.right = v3;

		rb.velocity += v2 * Time.deltaTime * ACCELERATION;
		if (rb.velocity.magnitude > MAX_SPEED){
			//Gradually reduce speed
			rb.velocity = ((rb.velocity.normalized*rb.velocity.magnitude) + (rb.velocity.normalized * MAX_SPEED))/2;
		}
	}

	public void OnColliderStay(Collider2D otherCollider)
    {
		//Only read every second, this adds some "REACTION TIME"
		if ((Time.frameCount+frameCountOffset) % 60 != 0)
			return;

		DateTime currTime =  DateTime.Now;
        Double timeDelta = (currTime - lastStateChange).TotalMilliseconds;
		

		//Shows what was seen
		var hit = otherCollider.gameObject;
		
		if (hit.tag == "Player")
		{
			var hitRb = hit.GetComponent<Rigidbody2D>();

			var heading = hit.transform.position - character.transform.position;
			var distance = heading.magnitude;
			var direction = heading / distance;

			//Only act if distance is closer than the lastDistance registered
			if (timeDelta > 500 && distance < lastDistance)
			{
				return;
			}

			if (distance > 3)
			{
				// Velocity is in units per second
				// since this is run every second this is a good estimate of where the player will be in 1 second
				futureGoalPos =  (Vector2)hit.transform.position + hitRb.velocity/2;
			}
			else
			{
				futureGoalPos = character.transform.position - direction;
			}

			state = "fighting";
			lastStateChange = DateTime.Now;
			lastDistance = distance;
		}
		else if (hit.tag == "Wall")
		{
			if (state == "fighting")
				return;

			var heading = hit.transform.position - character.transform.position;
			var distance = heading.magnitude;
			var direction = heading / distance;

			futureGoalPos = character.transform.position - direction; //Move away from the wall

			state = "avoiding";
			lastStateChange = DateTime.Now;
			lastDistance = 0;
		}
    }
}
