﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armour : MonoBehaviour {
	public Sprite[] sprites;
	void Start () {
		GetComponent<SpriteRenderer>().sprite = sprites[(int)Random.Range(0.0f, (float)sprites.Length)];
	}
}
