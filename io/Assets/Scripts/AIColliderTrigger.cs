﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIColliderTrigger : MonoBehaviour {

	// Use this for initialization
	void OnTriggerStay2D(Collider2D collider){
		SendMessageUpwards("OnColliderStay", collider);
	}
}
