﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodEffect : MonoBehaviour {

	public Sprite[] initialSprites;
	public Sprite[] splatterSprites;
	SpriteRenderer sp;
	public float speed;

	void stop () {
		speed = 0;
		sp.sprite = splatterSprites[(int)(Random.Range(0.0f, (float)splatterSprites.Length))];
	}

	void remove () {
		Destroy(gameObject);
	}

	void Start () {
		sp = GetComponent<SpriteRenderer>();
		sp.sprite = initialSprites[(int)(Random.Range(0.0f, (float)initialSprites.Length))];
		Invoke("stop", 0.3f + Random.Range(0.0f, 0.8f));
		Invoke("remove", 10f);
	}

	void Update () {
		transform.Translate(Vector2.right * Time.deltaTime * speed);
		speed = speed * 0.99f;
	}
}
