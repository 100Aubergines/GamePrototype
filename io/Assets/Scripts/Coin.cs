﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Coin : NetworkBehaviour {

	public Sprite[] sprites;
	int currentSprite = 0;
	SpriteRenderer sp;

	void changeSprite() {
		currentSprite++;
		if(currentSprite >= sprites.Length){
			currentSprite = 0;
		}
		sp.sprite = sprites[currentSprite];
		Invoke("changeSprite", 0.5f);
	}

	void Start () {
		sp = GetComponent<SpriteRenderer>();
		sp.sprite = sprites[0];
		Invoke("changeSprite", 0.5f);
		Invoke("remove", 60f);
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(isServer){
			var hit = collider.gameObject;
			if (hit.tag == "Player"){
				collider.attachedRigidbody.gameObject.transform.parent.GetComponent<CharController>().coins++;
				collider.attachedRigidbody.gameObject.transform.parent.GetComponent<CharController>().RpcLengthenSword(1);
				Destroy(gameObject);
			}
		}
	}

	void remove () {
		Destroy(gameObject);
	}
}
