﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;


public class NetManBehaviour : NetworkManager {
	public string playerName;

	
	public override void OnClientError(NetworkConnection connection, int errorCode)
    {
    	SceneManager.LoadScene("menu", LoadSceneMode.Single);
		Debug.Log(errorCode);

    }
	public override void OnClientDisconnect(NetworkConnection connection)
    {
		SceneManager.LoadScene("menu", LoadSceneMode.Single);

    }
}
